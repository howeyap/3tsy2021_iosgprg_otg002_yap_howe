﻿using UnityEngine;

public class WeaponGiver : MonoBehaviour
{
    [SerializeField]
    private Gun[] guns;

    private void Awake()
    {
        GameManager.MyInstance.theGiver = this;
    }

    public Gun GetGun(GunType requested)
    {
        for (int i = 0; i < guns.Length; i++)
        {
            if (requested == guns[i].GetGunType())
            {
                return (Gun)Instantiate(guns[i]);
            }
        }
        return null;
    }

    public Gun[] GetGunArray()
    {
        return guns;
    }
}
