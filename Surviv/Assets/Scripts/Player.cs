﻿using UnityEngine;
using UnityEngine.UI;

public delegate void OnDeath();
public class Player : Unit
{
    public event OnDeath onDeath;

    [SerializeField]
    private Joystick moveJoystick;

    [SerializeField]
    private Joystick fireJoystick;

    private UIManager UIMan;

    private bool isFiring;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        UIMan = GameManager.MyInstance.theUIManager;
    }

    private void Awake()
    {
        GameManager.MyInstance.thePlayer = this;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Rotate(moveJoystick.Horizontal, moveJoystick.Vertical) && !isFiring)
        {
            MoveInput = transform.up * MoveSpeed * Time.deltaTime * 30;
        }
        else if (Mathf.Abs(moveJoystick.Horizontal) > .2f || Mathf.Abs(moveJoystick.Vertical) > .2f && isFiring)
        {
            MoveInput = new Vector2(moveJoystick.Horizontal, moveJoystick.Vertical);
        }
        else
        {
            rb.velocity = Vector2.zero;
        }

        if (Rotate(fireJoystick.Horizontal, fireJoystick.Vertical) && equipped != null)
        {
            isFiring = true;
            equipped.Fire(firePoint);
            UIMan.EquippedAmmoChanged(equipped.CurrentAmmo, equipped.MaxAmmo);
        }
        else
        {
            isFiring = false;
        }

        if (equipped != null)
        {
            UIMan.EquippedAmmoChanged(equipped.CurrentAmmo, equipped.MaxAmmo);
        }
    }

    bool Rotate(float x, float y)
    {
        if (Mathf.Abs(x) > .2f || Mathf.Abs(y) > .2f)
        {
            float direction = Mathf.Atan2(x * -1, y);
            direction *= Mathf.Rad2Deg;
            rb.MoveRotation(direction);
            return true;
        }
        return false;
    }

    public void Reload()
    {
        if (equipped != null)
        {
            equipped.Reload();
        }
    }

    public override void Reloading()
    {
        UIMan.Reloading();
    }
    public override void DoneReload()
    {
        UIMan.DoneReload();
        UIMan.EquippedAmmoChanged(equipped.CurrentAmmo, equipped.MaxAmmo);
    }
    public override void AddAmmo(AmmoType ammoType, int ammoCount)
    {
        UIMan.AmmoChanged(ammoType, ammoCount);
    }
    public override void DeductedAmmo(AmmoType ammoType, int ammoCount)
    {
        UIMan.AmmoChanged(ammoType, ammoCount);
    }

    public override void AddGun(Gun loot)
    {
        loot.SetUser(this);
        UIMan.AddGun(loot, HasGun);
    }

    public override void TakeDamage(int dmg)
    {
        base.TakeDamage(dmg);
        UIMan.HealthChanged((float)myCurrentHealth / (float)myMaxHealth);
        if (myCurrentHealth <= 0)
        {
            onDeath();
        }
    }
}
