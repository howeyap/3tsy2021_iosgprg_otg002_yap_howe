﻿using UnityEngine;

public class Rifle : Gun
{
    [SerializeField]
    private int recoil;
    public override void Start()
    {
        base.Start();
    }
    public override void Fire(Transform firePoint)
    {
        if (!isFiring && CurrentAmmo > 0 && !reloading)
        {
            intervalCounter = fireInterval;
            isFiring = true;
            GameObject myBullet = Instantiate(bullet, firePoint.position, firePoint.parent.rotation * Quaternion.Euler(0,0,Random.Range(-recoil, recoil)));
            myBullet.GetComponent<Bullet>().SetDamage(myDamage);
            DeductAmmo(1);
        }
        else if (CurrentAmmo <= 0 && !reloading && inventory.GetAmmo(AmmoType) > 0)
        {
            Reload();
        }
        else if (!reloading)
        {
            intervalCounter -= Time.deltaTime;
            if (intervalCounter <= 0)
            {
                isFiring = false;
            }
        }
    }
}
