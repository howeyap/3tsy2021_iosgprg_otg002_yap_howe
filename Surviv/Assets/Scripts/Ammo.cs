﻿using UnityEngine;
using UnityEngine.UI;

public class Ammo : Lootable
{
    [SerializeField]
    private Text ammoText;

    [SerializeField]
    private string ammoType;
    public string AmmoType
    {
        get
        {
            return ammoType;
        }
    }

    [SerializeField]
    private int ammoCount;
    public int AmmoCount
    {
        get
        {
            return ammoCount;
        }
    }
    public void DeductAmmo(int lostAmmo)
    {
        ammoCount -= lostAmmo;
        if (ammoCount <= 0)
        {
            ammoCount = 0;
        }

        ammoText.text = ammoCount.ToString();
    }

    public void AddAmmo(int newAmmo)
    {
        ammoCount += newAmmo;
        ammoText.text = ammoCount.ToString();
    }
}
