﻿
using UnityEngine;

public class Lootable : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.transform.GetComponent<Inventory>().Add(this);
        }
    }
}
