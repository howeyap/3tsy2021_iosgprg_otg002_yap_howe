﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject enemyPrefab;

    [SerializeField]
    private int numOfEnemies;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < numOfEnemies; i++)
        {
            Instantiate(enemyPrefab);
        }
    }
}
