﻿using UnityEngine;

public class Enemy : Unit
{
    private IState currentState;

    [SerializeField]
    private float initAggroRange;

    [SerializeField]
    private float myAttackRange;
    public float MyAttackRange
    {
        get
        {
            return myAttackRange;
        }
    }
    public float MyAggroRange { get; set; }
    public Transform MyTarget { get; set; }
    public bool IsAttacking 
    {
        get
        {
            if (equipped != null)
            {
                return equipped.IsFiring();
            }
            return false;
        }
    }
    public bool InRange
    {
        get
        {
            return Vector2.Distance(transform.position, MyTarget.position) < MyAggroRange;
        }
    }

    public Gun myGun
    {
        get
        {
            return equipped;
        }
    }

    public Transform myFirePoint
    {
        get
        {
            return firePoint;
        }
    }

    [SerializeField]
    private float maxRotateSpeed;

    public Vector3 MyStartPosition { get; set; }

    public Transform lootTarget;

    public bool stuckOnObstacle;

    protected override void Start()
    {
        base.Start();
        ChangeState(new IdleState());
        MyAggroRange = initAggroRange;
        //GetInventory().Add(new Pistol);

        RandomInit();
    }

    private void RandomInit()
    {
        Random.Range(4, maxRotateSpeed);
        int randGun = Random.Range(0, GameManager.MyInstance.theGiver.GetGunArray().Length);
        AddGun(GameManager.MyInstance.theGiver.GetGun(GameManager.MyInstance.theGiver.GetGunArray()[randGun].GetGunType()));
        MoveSpeed = Random.Range(2, MoveSpeed);
    }

    private void Update()
    {
        currentState.Update();
    }
    public void ChangeState(IState newState)
    {
        if (currentState != null)
        {
            currentState.Exit();
        }

        currentState = newState;

        currentState.Enter(this); //reference myself and enter new state
    }

    public void SetTarget(Transform target)
    {
        if (MyTarget == null)
        {
            float distance = Vector2.Distance(transform.position, target.position);
            MyAggroRange = initAggroRange;
            MyAggroRange += distance;
            MyTarget = target;
        }
    }
    public void SetLoot(Transform target)
    {
        if (lootTarget == null)
        {
            float distance = Vector2.Distance(transform.position, target.position);
            MyAggroRange = initAggroRange;
            MyAggroRange += distance;
            lootTarget = target;
        }
    }
    public void Reset()
    {
        this.MyTarget = null;
        this.MyAggroRange = initAggroRange;
        lootTarget = null;
    }

    public float getRotateSpeed()
    {
        return maxRotateSpeed;
    }

    public override void Die()
    {
        Destroy(transform.gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Obstacle"))
        {
            stuckOnObstacle = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Obstacle"))
        {
            stuckOnObstacle = false;
        }
    }

    public void RemoveLoot()
    {
        Destroy(lootTarget.gameObject);
        lootTarget = null;
    }
}
