﻿using UnityEngine;

public class LootState : IState
{
    private Enemy parent;
    public void Enter(Enemy parent)
    {
        this.parent = parent;
    }

    public void Exit()
    {

    }

    void IState.Update()
    {
        if (parent.lootTarget != null)
        {
            parent.MoveInput = (parent.lootTarget.transform.position - parent.transform.position).normalized;
            parent.transform.position = Vector2.MoveTowards(parent.transform.position, parent.lootTarget.position, parent.MoveSpeed * Time.deltaTime);
            float dir = Mathf.Atan2(parent.lootTarget.transform.position.x - parent.transform.position.x, parent.lootTarget.transform.position.y - parent.transform.position.y);
            dir *= Mathf.Rad2Deg * -1;
            parent.transform.rotation = Quaternion.Lerp(parent.transform.rotation, Quaternion.Euler(0, 0, dir), parent.getRotateSpeed() * Time.deltaTime);

        }

        if (parent.MyTarget != null)
        {
            parent.ChangeState(new FollowState());
        }
        if (parent.stuckOnObstacle)
        {
            parent.ChangeState(new AvoidObstacleState());
        }

        if (parent.lootTarget == null)
        {
            parent.ChangeState(new IdleState());
        }
    }
}
