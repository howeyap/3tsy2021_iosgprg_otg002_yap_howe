﻿using UnityEngine;

public class EnemyRange : MonoBehaviour
{

    private Enemy parent;

    private void Awake()
    {
        parent = GetComponentInParent<Enemy>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Unit") || collision.CompareTag("Player"))
        {
            if (collision != null)
            {
                parent.SetTarget(collision.transform);
            }
        }
        else if (collision.GetComponent<Loot>() != null)
        {
            parent.SetLoot(collision.transform);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Unit") || collision.CompareTag("Player"))
        {
            if (collision != null)
            {
                parent.SetTarget(collision.transform);
            }
        }
        else if (collision.GetComponent<Loot>() != null)
        {
            parent.SetLoot(collision.transform);
        }
    }
}
