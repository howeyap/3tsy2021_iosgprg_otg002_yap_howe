﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class PatrolState : IState
{
    private Enemy parent;

    private Vector3 destination;
    public void Enter(Enemy parent)
    {
        this.parent = parent;

        destination = new Vector3(parent.transform.position.x + Random.Range(-10, 10), parent.transform.position.y + Random.Range(-10, 10), 0);
    }

    public void Exit()
    {
        parent.MoveInput = Vector2.zero;
    }

    void IState.Update()
    {
        parent.MoveInput = (destination - parent.transform.position).normalized;
        parent.transform.position = Vector2.MoveTowards(parent.transform.position, destination, parent.MoveSpeed * Time.deltaTime);
        float dir = Mathf.Atan2(destination.x - parent.transform.position.x, destination.y - parent.transform.position.y);
        dir *= Mathf.Rad2Deg * -1;
        parent.transform.rotation = Quaternion.Lerp(parent.transform.rotation, Quaternion.Euler(0, 0, dir), parent.getRotateSpeed() * Time.deltaTime);

        if (Vector3.Distance(parent.transform.position, destination) < 1)
        {
            parent.ChangeState(new IdleState());
        }
        if (parent.MyTarget != null)
        {
            parent.ChangeState(new FollowState());
        }
        else if (parent.lootTarget != null)
        {
            parent.ChangeState(new LootState());
        }
    }
}
