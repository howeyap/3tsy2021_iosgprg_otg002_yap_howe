﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : IState
{
    private Enemy parent;

    private float extraRange = .5f;

    public void Enter(Enemy parent)
    {
        this.parent = parent;
    }

    public void Exit()
    {

    }

    public void Update()
    {
        Attack();

        if (parent.MyTarget != null)
        {
            float dir = Mathf.Atan2(parent.MyTarget.transform.position.x - parent.transform.position.x, parent.MyTarget.transform.position.y - parent.transform.position.y);
            dir *= Mathf.Rad2Deg * -1;
            parent.transform.rotation = Quaternion.Lerp(parent.transform.rotation, Quaternion.Euler(0, 0, dir), parent.getRotateSpeed() * Time.deltaTime);
            float distance = Vector2.Distance(parent.MyTarget.position, parent.transform.position);

            if (distance >= parent.MyAttackRange + extraRange && !parent.IsAttacking)
            {
                parent.ChangeState(new FollowState());
            }
        }
        else
        {
            parent.ChangeState(new IdleState());
        }
    }

    public void Attack()
    {
        if (parent.myGun != null)
        {
            parent.myGun.Fire(parent.myFirePoint);
        }
    }
}
