﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvoidObstacleState : IState
{
    private Enemy parent;
    public void Enter(Enemy parent)
    {
        this.parent = parent;
        parent.RemoveLoot();
    }

    public void Exit()
    {

    }

    void IState.Update()
    {
        if (parent.lootTarget != null)
        {
            //parent.MoveInput = (parent.lootTarget.transform.position - parent.transform.position).normalized;
            //parent.transform.position = Vector2.MoveTowards(parent.transform.position, parent.lootTarget.position, parent.MoveSpeed * Time.deltaTime);
            //float dir = Mathf.Atan2(parent.lootTarget.transform.position.x - parent.transform.position.x -1, parent.lootTarget.transform.position.y - parent.transform.position.y -1);
            //dir *= Mathf.Rad2Deg * -1;
            //parent.transform.rotation = Quaternion.Lerp(parent.transform.rotation, Quaternion.Euler(0, 0, dir), parent.getRotateSpeed() * Time.deltaTime);

            //float distance = Vector2.Distance(parent.lootTarget.position, parent.transform.position);
        }

        if (parent.MyTarget != null)
        {
            parent.ChangeState(new FollowState());
        }
        if (!parent.stuckOnObstacle)
        {
            parent.ChangeState(new LootState());
        }

        if (parent.lootTarget == null)
        {
            parent.ChangeState(new IdleState());
        }
    }
}
