﻿
using UnityEngine;

public class Loot : MonoBehaviour
{
    [SerializeField]
    private GunType myGunType;

    [SerializeField]
    private AmmoType myAmmoType;

    [SerializeField]
    private int myAmmoCount;

    [SerializeField]
    private bool isGun;

    private bool canSwap = false;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Unit"))
        {
            if (isGun)
            {
                Gun tmp = GameManager.MyInstance.theGiver.GetGun(myGunType);
                if (collision.GetComponent<Inventory>().AddGun(tmp))
                {
                    Destroy(gameObject);
                }
                else
                {
                    Destroy(tmp);
                    Destroy(gameObject);
                }
            }
            else
            {
                collision.GetComponent<Inventory>().AddAmmo(myAmmoType, myAmmoCount);
                Destroy(gameObject);
            }
        }

        if (collision.CompareTag("Player"))
        {
            if (isGun)
            {
                Gun tmp = GameManager.MyInstance.theGiver.GetGun(myGunType);
                if (collision.GetComponent<Inventory>().AddGun(tmp))
                {
                    Destroy(gameObject);
                }
                else if (collision.GetComponent<Player>() != null)
                {
                    GameManager.MyInstance.theUIManager.SwapGun(GameManager.MyInstance.theGiver.GetGun(myGunType), transform.gameObject);
                }
                else
                {
                    Destroy(tmp);
                }
            }
            else
            {
                collision.GetComponent<Inventory>().AddAmmo(myAmmoType, myAmmoCount);
                Destroy(gameObject);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<Player>() != null)
        {
            GameManager.MyInstance.theUIManager.CloseSwap();
        }
    }
    public AmmoType GetAmmoType()
    {
        return myAmmoType;
    }
    public GunType GetGunType()
    {
        return myGunType;
    }
}
