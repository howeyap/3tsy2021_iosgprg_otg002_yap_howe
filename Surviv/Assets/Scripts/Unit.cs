﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Inventory))]
public abstract class Unit : MonoBehaviour
{
    [SerializeField]
    protected int myMaxHealth;

    protected int myCurrentHealth;

    private Vector2 moveInput;
    public Vector2 MoveInput
    {
        get
        {
            return moveInput;
        }
        set
        {
            moveInput = value;
        }
    }

    [SerializeField]
    private float moveSpeed;
    public float MoveSpeed
    {
        get
        {
            return moveSpeed;
        }

        set
        {
            moveSpeed = value;
        }
    }

    [SerializeField]
    protected Gun equipped;

    [SerializeField]
    protected Transform firePoint;

    protected Rigidbody2D rb;

    [SerializeField]
    private SpriteRenderer spriteEquipped;

    private Inventory myInventory;

    [SerializeField]
    private GameObject bloodSplat;

    public bool HasGun
    {
        get
        {
            return equipped != null;
        }
    }
    // Start is called before the first frame update
    protected virtual void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        myInventory = GetComponent<Inventory>();
        myInventory.SetUser(this);
        myCurrentHealth = myMaxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    private void Move()
    {
        rb.velocity = new Vector2(MoveInput.normalized.x * moveSpeed, MoveInput.normalized.y * moveSpeed);
    }
    public void Equip(Gun gun)
    {
        equipped = gun;
        spriteEquipped.sprite = equipped.MySprite;
    }

    public Inventory GetInventory()
    {
        return myInventory;
    }

    public virtual void TakeDamage(int dmg)
    {
        GameObject blood = Instantiate(bloodSplat, transform.position, Quaternion.identity);
        Destroy(blood, 1f);
        myCurrentHealth -= dmg;
        if (myCurrentHealth <= 0)
        {
            myCurrentHealth = 0;
            Die();
        }
    }

    public virtual void Die()
    {
    }

    public virtual void Reloading()
    {
    }
    public virtual void DoneReload()
    {
    }

    public virtual void AddGun(Gun loot)
    {
        loot.SetUser(this);

        if (!HasGun)
        {
            Equip(loot);
        }
    }
    public virtual void AddAmmo(AmmoType ammoType, int ammoCount)
    {
    }
    public virtual void DeductedAmmo(AmmoType ammoType, int ammoCount)
    {
    }
}
