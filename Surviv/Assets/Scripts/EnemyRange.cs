﻿using UnityEngine;

public class EnemyRange : MonoBehaviour
{

    private Enemy parent;

    private void Start()
    {
        parent = GetComponentInParent<Enemy>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            parent.SetTarget(collision.transform);
        }
    }
}
