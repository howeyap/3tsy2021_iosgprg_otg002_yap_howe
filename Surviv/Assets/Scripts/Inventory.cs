﻿using System;
using UnityEngine;

public enum AmmoType { Blue, Red, Orange }
public class Inventory : MonoBehaviour
{
    [SerializeField]
    private Gun main, secondary;

    [Serializable]
    private struct Ammunition
    {
        public int myAmmoCount;
        public AmmoType myAmmoType;
    }

    [SerializeField]
    private Ammunition[] ammoSlots;

    private Unit myUser;

    public bool AddGun(Gun loot)
    {
        if (main == null && !(loot is Pistol))
        {
            main = loot;
            myUser.AddGun(loot);
            return true;
        }

        if (secondary == null && loot is Pistol)
        {
            secondary = loot;
            myUser.AddGun(loot);
            return true;
        }
        return false;
    }

    public Gun SwapGun(Gun loot)
    {
        if (!(loot is Pistol))
        {
            Gun tmp = main;
            main = loot;
            myUser.AddGun(loot);
            myUser.Equip(loot);
            return tmp;
        }
        else if (loot is Pistol)
        {
            Gun tmp = secondary;
            secondary = loot;
            myUser.AddGun(loot);
            myUser.Equip(loot);
            return tmp;
        }
        return null;
    }

    public void AddAmmo(AmmoType ammoType, int ammoCount)
    {

        for (int i = 0; i < ammoSlots.Length; i++)
        {
            if (ammoType == ammoSlots[i].myAmmoType)
            {
                ammoSlots[i].myAmmoCount += ammoCount;
                myUser.AddAmmo(ammoType, ammoSlots[i].myAmmoCount);
                return;
            }
        }
    }

    public void DeductAmmo(AmmoType ammoType, int deduct)
    {
        for (int i = 0; i < ammoSlots.Length; i++)
        {
            if (ammoType == ammoSlots[i].myAmmoType)
            {
                ammoSlots[i].myAmmoCount -= deduct;
                myUser.DeductedAmmo(ammoType, ammoSlots[i].myAmmoCount);
            }
        }
    }
    public int GetAmmo(AmmoType ammoType)
    {
        for (int i = 0; i < ammoSlots.Length; i++)
        {
            if (ammoType == ammoSlots[i].myAmmoType)
            {
                return ammoSlots[i].myAmmoCount;
            }
        }

        return 0;
    }

    public void SetUser(Unit unit)
    {
        myUser = unit;
    }
}
