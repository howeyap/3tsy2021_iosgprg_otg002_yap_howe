﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    private int myDamage;

    public void SetDamage(int dmg)
    {
        myDamage = dmg;
    }
    void Start()
    {
        Destroy(gameObject, 1f);
    }

    private void Update()
    {
        GetComponent<Rigidbody2D>().AddForce(transform.up, ForceMode2D.Impulse);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Unit unit = collision.GetComponent<Unit>();

        if (collision.gameObject.tag.Contains("Obstacle"))
        {
            Destroy(gameObject);
        }

        if (unit != null)
        {
            unit.TakeDamage(myDamage);
            Destroy(gameObject);
        }
    }
}