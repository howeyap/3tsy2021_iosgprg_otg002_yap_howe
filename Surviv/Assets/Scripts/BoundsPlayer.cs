﻿using UnityEngine;

public class BoundsPlayer : MonoBehaviour
{
    private BoxCollider2D boundBox;
    private Vector3 maxBounds;
    private Vector3 minBounds;

    private float halfHeight;
    private float halfWidth;

    void Start()
    {
        boundBox = GameManager.MyInstance.worldBounds;
        minBounds = boundBox.bounds.min;
        maxBounds = boundBox.bounds.max;

        halfHeight = transform.GetComponent<SpriteRenderer>().bounds.size.y/2;
        halfWidth = transform.GetComponent<SpriteRenderer>().bounds.size.x/2;

        transform.position = new Vector3(Random.Range(boundBox.bounds.min.x, boundBox.bounds.max.x), Random.Range(boundBox.bounds.min.y, boundBox.bounds.max.y),0);
    }

    void LateUpdate()
    {

        float clampedX = Mathf.Clamp(transform.position.x, minBounds.x + halfWidth, maxBounds.x - halfWidth);
        float clampedY = Mathf.Clamp(transform.position.y, minBounds.y + halfHeight, maxBounds.y - halfHeight);
        transform.position = new Vector3(clampedX, clampedY, transform.position.z);
    }
}
