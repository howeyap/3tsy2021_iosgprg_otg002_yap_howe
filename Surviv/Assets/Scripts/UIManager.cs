﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private GameObject reloadText;

    [SerializeField]
    private Text equippedAmmoText;

    [System.Serializable]
    private struct AmmoText
    {
        public Text myAmmoText;
        public AmmoType myAmmoType;
    }

    [SerializeField]
    private AmmoText[] ammoTexts;

    [System.Serializable]
    public struct GunSlot
    {
        public Text myGunName;
        public Image myIcon;
        public GameObject selectionOutline;
        public Gun myGun;
    }

    [SerializeField]
    private GunSlot main, secondary;

    private Player thePlayer;

    [SerializeField]
    private GameObject swapButton;

    [SerializeField]
    private Loot[] swaps;

    [SerializeField]
    private Image healthBar;

    [SerializeField]
    private GameObject gameOverScreen;
    private void Awake()
    {
        GameManager.MyInstance.theUIManager = this;
    }

    private void Start()
    {
        thePlayer = GameManager.MyInstance.thePlayer;
        thePlayer.onDeath += new OnDeath(GameOver);
    }
    public void Reloading()
    {
        reloadText.SetActive(true);
    }
    public void DoneReload()
    {
        reloadText.SetActive(false);
    }

    public void EquippedAmmoChanged(int current, int max)
    {
        equippedAmmoText.text = current.ToString() + " / " + max.ToString();
    }

    public void AmmoChanged(AmmoType ammoType, int newAmmo)
    {
        for (int i = 0; i < ammoTexts.Length; i++)
        {
            if (ammoTexts[i].myAmmoType == ammoType)
            {
                ammoTexts[i].myAmmoText.text = newAmmo.ToString();
            }
        }
    }

    public void ReloadButton()
    {
        GameManager.MyInstance.thePlayer.Reload();
    }

    public void AddGun(Gun loot, bool hasGun)
    {
        if (loot.GetGunType() != GunType.Pistol)
        {
            main.myGun = loot;
            AddToSlot(loot, main);
            SelectMain();
        }
        else
        {
            secondary.myGun = loot;
            AddToSlot(loot, secondary);
            SelectSecondary();
        }
    }

    public void SelectMain()
    {
        main.selectionOutline.SetActive(true);
        thePlayer.Equip(main.myGun);
        secondary.selectionOutline.SetActive(false);
    }
    public void SelectSecondary()
    {
        secondary.selectionOutline.SetActive(true);
        thePlayer.Equip(secondary.myGun);
        main.selectionOutline.SetActive(false);
    }

    void AddToSlot(Gun loot, GunSlot slot)
    {
        slot.myGunName.text = loot.MyName;
        slot.myIcon.color = Color.white;
        slot.myIcon.sprite = loot.MyIcon;
    }

    public void SwapGun(Gun newGun, GameObject lootObject)
    {
        swapObject = lootObject;
        upForSwap = newGun;
        swapButton.SetActive(true);
    }

    private Gun upForSwap;
    private GameObject swapObject;

    public void SwapPressed()
    {
        Gun tmp = thePlayer.GetComponent<Inventory>().SwapGun(Instantiate(upForSwap.GetComponent<Gun>()));
        Destroy(swapObject);

        for (int i = 0; i < swaps.Length; i++)
        {
            if (tmp.GetGunType() == swaps[i].GetGunType())
            {
                //Instantiate(swaps[i], thePlayer.transform.position, Quaternion.identity);
                Destroy(tmp);
            }
        }

        CloseSwap();
        upForSwap = null;
    }

    public void CloseSwap()
    {
        swapButton.SetActive(false);
        Destroy(upForSwap);
    }

    public void HealthChanged(float fill)
    {
        healthBar.fillAmount = fill;
    }

    public void GameOver()
    {
        gameOverScreen.SetActive(true);
    }
}
