﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Unit
{
    private IState currentState;

    [SerializeField]
    private float initAggroRange;
    public float MyAggroRange { get; set; }
    public Transform MyTarget { get; set; }
    public bool InRange
    {
        get
        {
            return Vector2.Distance(transform.position, MyTarget.position) < MyAggroRange;
        }
    }

    public Vector3 MyStartPosition { get; set; }

    protected override void Start()
    {
        base.Start();
        ChangeState(new IdleState());
    }

    private void Update()
    {
        currentState.Update();
    }
    public void ChangeState(IState newState)
    {
        if (currentState != null)
        {
            currentState.Exit();
        }

        currentState = newState;

        currentState.Enter(this); //reference myself and enter new state
    }

    public void SetTarget(Transform target)
    {
        if (MyTarget == null)
        {
            float distance = Vector2.Distance(transform.position, target.position);
            MyAggroRange = initAggroRange;
            MyAggroRange += distance;
            MyTarget = target;
        }
    }
    public void Reset()
    {
        this.MyTarget = null;
        this.MyAggroRange = initAggroRange;
    }
}
