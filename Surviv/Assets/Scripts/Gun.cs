﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
public enum GunType { Pistol, Shotgun, Rifle }
public abstract class Gun : MonoBehaviour
{
    [SerializeField]
    private GunType myGunType;

    protected bool reloading;

    protected Inventory inventory;

    private Unit myUser;

    [SerializeField]
    private string myName;

    public string MyName
    {
        get
        {
            return myName;
        }
    }
    [SerializeField]
    private Sprite icon;

    public Sprite MyIcon
    {
        get
        {
            return icon;
        }
    }

    [SerializeField]
    private Sprite mySprite;
    public Sprite MySprite
    {
        get
        {
            return mySprite;
        }
    }

    [SerializeField]
    private int maxAmmo;
    public int MaxAmmo
    {
        get
        {
            return maxAmmo;
        }
    }

    private int curAmmo;
    public int CurrentAmmo
    {
        get
        {
            return curAmmo;
        }
    }

    [SerializeField]
    private AmmoType ammoType;
    public AmmoType AmmoType
    {
        get
        {
            return ammoType;
        }
    }

    [SerializeField]
    protected GameObject bullet;

    protected bool isFiring;

    protected float intervalCounter;

    [SerializeField]
    protected float fireInterval;

    [SerializeField]
    protected float reloadTime;

    [SerializeField]
    protected int myDamage;
    public virtual void Fire(Transform firePoint)
    {
        Debug.Log("FIRE");
    }

    public void SetUser(Unit user)
    {
        inventory = user.GetInventory();
        myUser = user;
    }
    public void Reload()
    {
        if (CurrentAmmo < MaxAmmo && !reloading && inventory.GetAmmo(AmmoType) > 0)
        {
            StartCoroutine(StartReload());
        }
    }

    public void DeductAmmo(int lostAmmo)
    {
        curAmmo -= lostAmmo;
        if (curAmmo <= 0)
        {
            curAmmo = 0;
        }
    }
    public void AddAmmo(int newAmmo)
    {
        curAmmo += newAmmo;
        if (curAmmo <= 0)
        {
            curAmmo = 0;
        }
    }

    public virtual void Start()
    {
        curAmmo = maxAmmo;
    }
    private IEnumerator StartReload()
    {
        if (!reloading)
        {
            reloading = true;
            myUser.Reloading();
            yield return new WaitForSeconds(reloadTime);
            CalculateAmmo();
            myUser.DoneReload();
            reloading = false;
        }
        yield return new WaitForEndOfFrame();
    }
    void CalculateAmmo()
    {
        if (inventory.GetAmmo(AmmoType) >= MaxAmmo - CurrentAmmo)
        {
            inventory.DeductAmmo(AmmoType, MaxAmmo - CurrentAmmo);
            AddAmmo(MaxAmmo - CurrentAmmo);
        }
        else
        {
            AddAmmo(inventory.GetAmmo(AmmoType));
            inventory.DeductAmmo(AmmoType, inventory.GetAmmo(AmmoType));
        }
    }

    public bool IsFiring()
    {
        return isFiring;
    }

    public GunType GetGunType()
    {
        return myGunType;
    }
}
