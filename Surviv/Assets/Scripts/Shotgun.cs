﻿using UnityEngine;

public class Shotgun : Gun
{
    [SerializeField]
    private int spreadBulletCount;

    [SerializeField]
    private int spreadOffset;
    public override void Start()
    {
        base.Start();
    }
    public override void Fire(Transform firePoint)
    {
        if (!isFiring && CurrentAmmo > 0 && !reloading)
        {
            FireShotgun(firePoint);
        }
        else if (CurrentAmmo <= 0 && !reloading && inventory.GetAmmo(AmmoType) > 0)
        {
            Reload();
        }
        else if (!reloading)
        {
            intervalCounter -= Time.deltaTime;
            if (intervalCounter <= 0)
            {
                isFiring = false;
            }
        }
    }

    void FireShotgun(Transform firePoint)
    {
        intervalCounter = fireInterval;
        isFiring = true;
        for (int i = 2-spreadBulletCount; i < spreadBulletCount-1; i++)
        {
            GameObject myBullet = Instantiate(bullet, firePoint.position, firePoint.parent.rotation * Quaternion.Euler(0,0,spreadOffset*i));
            myBullet.GetComponent<Bullet>().SetDamage(myDamage);
        }
        DeductAmmo(1);
    }
}
