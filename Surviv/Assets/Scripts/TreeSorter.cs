﻿using UnityEngine;

public class TreeSorter : MonoBehaviour
{

    private SpriteRenderer myRenderer;

    private Color treeColor;
    private Color storedColor;
    // Use this for initialization
    void Start()
    {
        myRenderer = transform.GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Overhead")
        {
            treeColor = collision.GetComponent<SpriteRenderer>().color;
            storedColor = treeColor;
            treeColor = new Color(treeColor.r, treeColor.b, treeColor.g, .5f);
            collision.GetComponent<SpriteRenderer>().color = treeColor;

        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Overhead")
        {
            treeColor = storedColor;
            collision.GetComponent<SpriteRenderer>().color = treeColor;
        }
    }
}
