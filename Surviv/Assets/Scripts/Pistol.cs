﻿using System.Collections;
using UnityEngine;

public class Pistol : Gun
{
    public override void Start()
    {
        base.Start();
    }
    public override void Fire(Transform firePoint)
    {
        if (!isFiring && CurrentAmmo > 0 && !reloading)
        {
            intervalCounter = fireInterval;
            isFiring = true;
            GameObject myBullet = Instantiate(bullet, firePoint.position, firePoint.parent.rotation);
            myBullet.GetComponent<Bullet>().SetDamage(myDamage);
            DeductAmmo(1);
        }
        else if (CurrentAmmo <= 0 && !reloading && inventory.GetAmmo(AmmoType) > 0)
        {
            Reload();
        }
        else if (!reloading)
        {
            intervalCounter -= Time.deltaTime;
            if (intervalCounter <= 0)
            {
                isFiring = false;
            }
        }
        //UIMan.AmmoUpdate(CurrentAmmo, MaxAmmo);
    }

}
