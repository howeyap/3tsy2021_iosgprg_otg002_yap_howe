﻿using UnityEngine;
using UnityEngine.UI;

public class GunSlot : MonoBehaviour
{
    private Gun myGun;

    [SerializeField]
    private Image iconUI;

    [SerializeField]
    private Text nameText;
    public bool IsEmpty
    {
        get
        {
            return myGun == null;
        }
    }

    public void AddGun(Gun gun)
    {
        myGun = gun;

        iconUI.sprite = myGun.MyIcon;
        iconUI.color = Color.white;
        nameText.text = myGun.MyName;
    }

    public void Equip(Unit myUser)
    {
        if (!IsEmpty)
        {
            myUser.Equip(myGun);
        }
    }
}
