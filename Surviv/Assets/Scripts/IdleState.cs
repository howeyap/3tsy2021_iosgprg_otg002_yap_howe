﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class IdleState : IState
{
    private Enemy parent;

    float idleTime;

    float waitTime = 2;
    public void Enter(Enemy parent)
    {
        this.parent = parent;

        this.parent.Reset();

        idleTime = 0;
    }

    public void Exit()
    {

    }

    public void Update()
    {
        idleTime += Time.deltaTime;

        if (parent.MyTarget != null)
        {
            parent.ChangeState(new FollowState());
        }

        if (idleTime > waitTime)
        {
            parent.ChangeState(new PatrolState());
        }
    }
}