﻿using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    [SerializeField]
    private Transform[] spawnAreas;

    [SerializeField]
    private GameObject[] weapons;

    [SerializeField]
    private GameObject[] ammos;
    void Start()
    {
        spawnAreas = GetComponentsInChildren<Transform>();
        Spawn();
    }

    void Spawn()
    {
        for (int i = 1; i < spawnAreas.Length; i++)
        {
            int randGun = Random.Range(0, weapons.Length);
            if (ToSpawn())
            {
                GameObject newGun = Instantiate(weapons[randGun], spawnAreas[i].position, Quaternion.identity, transform);
                SpawnCorrespondingAmmo(newGun.GetComponent<Loot>().GetAmmoType(), i);
            }
            else
            {
                if (ToSpawn())
                {
                    int randBullet = Random.Range(0, weapons.Length);
                    Instantiate(ammos[randBullet], spawnAreas[i].position, Quaternion.identity, transform);
                }
            }
        }
    }

    bool ToSpawn()
    {
        if (Random.Range(0,2) == 1)
        {
            return true;
        }

        return false;
    }

    int RandOffset()
    {
        int rand = 0;
        while (rand == 0)
        {
            rand = Random.Range(-1, 2);
        }
        return rand;
    }

    void SpawnCorrespondingAmmo(AmmoType ammoType, int index)
    {
        for (int i = 0; i < ammos.Length; i++)
        {
            if (ammoType == ammos[i].GetComponent<Loot>().GetAmmoType())
            {
                GameObject firstBullet = Instantiate(ammos[i], new Vector3(spawnAreas[index].position.x + RandOffset(), spawnAreas[index].position.y + RandOffset(), 0), Quaternion.identity, transform);
                Instantiate(ammos[i], new Vector3(firstBullet.transform.position.x + RandOffset() +1, firstBullet.transform.position.y + RandOffset(), 0), Quaternion.identity, transform);
            }
        }
    }
}
