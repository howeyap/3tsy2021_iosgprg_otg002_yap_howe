﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class PatrolState : IState
{
    private Enemy parent;

    private Vector3 destination;
    public void Enter(Enemy parent)
    {
        this.parent = parent;

        destination = new Vector3(parent.transform.position.x + Random.Range(-5, 5), parent.transform.position.y + Random.Range(-5, 5), 0);
    }

    public void Exit()
    {
        parent.MoveInput = Vector2.zero;
    }

    void IState.Update()
    {
        parent.MoveInput = (destination - parent.transform.position).normalized;
        parent.transform.position = Vector2.MoveTowards(parent.transform.position, destination, parent.MoveSpeed * Time.deltaTime);

        if (Vector3.Distance(parent.transform.position, destination) < 1)
        {
            parent.ChangeState(new IdleState());
        }
        if (parent.MyTarget != null)
        {
            parent.ChangeState(new FollowState());
        }
    }
}
