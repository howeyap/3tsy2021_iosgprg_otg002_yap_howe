﻿using UnityEngine;

public class CameraController : MonoBehaviour
{

    public GameObject player;
    private Vector3 targetPos;

    [SerializeField]
    private float moveSpeed;

    private BoxCollider2D boundBox;
    private Vector3 maxBounds;
    private Vector3 minBounds;

    private Camera theCamera;
    private float halfHeight;
    private float halfWidth;

    void Start()
    {
        boundBox = GameManager.MyInstance.worldBounds;
        minBounds = boundBox.bounds.min;
        maxBounds = boundBox.bounds.max;

        theCamera = GetComponent<Camera>();
        halfHeight = theCamera.orthographicSize;
        halfWidth = halfHeight * Screen.width / Screen.height;
    }

    void LateUpdate()
    {
        targetPos = new Vector3(player.transform.position.x, player.transform.position.y, transform.position.z);
        transform.position = Vector3.Lerp(transform.position, targetPos, moveSpeed * Time.deltaTime);

        float clampedX = Mathf.Clamp(transform.position.x, minBounds.x + halfWidth, maxBounds.x - halfWidth);
        float clampedY = Mathf.Clamp(transform.position.y, minBounds.y + halfHeight, maxBounds.y - halfHeight);
        transform.position = new Vector3(clampedX, clampedY, transform.position.z);
    }
}
