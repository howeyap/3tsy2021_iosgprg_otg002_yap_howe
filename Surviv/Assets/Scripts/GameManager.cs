﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;

    public static GameManager MyInstance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameManager>();
            }
            return instance;
        }
    }

    public UIManager theUIManager;

    public Player thePlayer;

    public WeaponGiver theGiver;

    public BoxCollider2D worldBounds;
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
    }
    private void Start()
    {
        thePlayer.onDeath += new OnDeath(Dead);
    }

    public void Dead()
    {
        thePlayer.gameObject.SetActive(false);
        Time.timeScale = 0;
    }
}
