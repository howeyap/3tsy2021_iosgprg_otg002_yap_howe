﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour
{
    private static Player instance;

    public static Player MyInstance
    {
        get
        {
            return instance;
        }
    }

    private float swipeStartTime;
    private float swipeEndTime;
    private float swipeTime;

    private Vector2 startSwipePosition;
    private Vector2 endSwipePosition;
    private float swipeLength;

    [SerializeField]
    private float maxSwipeTime;

    [SerializeField]
    private float minSwipeDistance;

    [HideInInspector]
    public bool isDashing;

    public List<Enemy> enemySwipe;

    private Vector3 startPos;

    [SerializeField]
    private int health;

    private Animator myAnim;

    private bool dead;

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
    }

    private void Awake()
    {
        myAnim = GetComponent<Animator>();

        GameManager.MyInstance.thePlayer = this;

        health = PlayerPrefs.GetInt("Health");
    }
    // Update is called once per frame
    void Update()
    {
        SwipeTest();

        //Test
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(Dash());
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            Swiped("Up");
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            Swiped("Down");
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            Swiped("Right");
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            Swiped("Left");
        }
    }

    void SwipeTest()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                swipeStartTime = Time.time;
                startSwipePosition = touch.position;
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                swipeEndTime = Time.time;
                endSwipePosition = touch.position;
                swipeTime = swipeEndTime - swipeStartTime;
                swipeLength = (endSwipePosition - startSwipePosition).magnitude;

                if (swipeTime<maxSwipeTime && swipeLength > minSwipeDistance)
                {
                    SwipeControl();
                }
                else if(!isDashing)
                {
                    StartCoroutine(Dash());
                }
            }
        }
    }

    void SwipeControl()
    {
        Vector2 distance = endSwipePosition - startSwipePosition;
        if (Mathf.Abs(distance.x) > Mathf.Abs(distance.y))
        {
            if (distance.x > 0)
            {
                Swiped("Right");
            }
            else if (distance.x < 0)
            {
                Swiped("Left");
            }
        }
        else if (Mathf.Abs(distance.y) > Mathf.Abs(distance.x))
        {
            if (distance.y > 0)
            {
                Swiped("Up");
            }
            else if (distance.y < 0)
            {
                Swiped("Down");
            }
        }
    }

    private IEnumerator Dash()
    {
        GameManager.MyInstance.AddScore(3);
        isDashing = true;
        myAnim.SetTrigger("Dash");
        yield return new WaitForSeconds(1f);
        isDashing = false;
        StopCoroutine(Dash());
    }

    void Swiped(string direction)
    {
        if (CheckEnemy())
        {
            if (direction == enemySwipe[0].myDirection)
            {
                StartCoroutine(Kill(enemySwipe[0].transform.position));
                enemySwipe[0].Damage(1);
            }
            else
            {
                Damage(1);
            }
        }
    }

    bool CheckEnemy()
    {
        if (enemySwipe.Count > 0)
        {
            if (enemySwipe[0].transform.position.y < enemySwipe[0].closeThreshold)
            {
                return true;
            }
            return false;
        }
        return false;
    }
    void Die()
    {
        StartCoroutine(GameManager.MyInstance.GameOver());
        dead = true;
    }

    private IEnumerator Kill(Vector3 enemyPos)
    {
        Vector3 newEnemyPos = new Vector3(enemyPos.x, enemyPos.y - .5f, enemyPos.z);
        myAnim.SetTrigger("Attack");
        //transform.position = Vector3.Lerp(transform.position, newEnemyPos, 20 * Time.deltaTime);
        transform.position = newEnemyPos;
        yield return new WaitForSeconds(.5f);
        transform.position = startPos;
        StopCoroutine(Kill(enemyPos));
    }

    public void Damage(int dmg)
    {
        if (health >= 1)
        {
            GameManager.MyInstance.RemoveHeart(health - 1);
        }

        health -= dmg;
        if (health <= 0 && !dead)
        {
            Die();
        }
    }

    public int GetHealth()
    {
        return health;
    }

    public void DeadEnemyByCollision()
    {
        enemySwipe[0].Damage(1) ;
    }
}
