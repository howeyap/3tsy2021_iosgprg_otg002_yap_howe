﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private CanvasGroup shop;

    [SerializeField]
    private CanvasGroup menu;
    public void Shop()
    {
        shop.alpha = (shop.alpha == 1)? 0 : 1;
        menu.alpha = (menu.alpha == 1)? 0 : 1;

        if (menu.blocksRaycasts)
        {
            menu.blocksRaycasts = false;
        }
        else
        {
            menu.blocksRaycasts = true;
        }

        if (shop.blocksRaycasts)
        {
            shop.blocksRaycasts = false;
        }
        else
        {
            shop.blocksRaycasts = true;
        }
    }

    public void StartGame()
    {
        SceneManager.LoadScene("Main"); 
    }

    public void Quit()
    {
        Application.Quit();
    }
    public void RestartValues()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.SetInt("Health", 1);
        PlayerPrefs.SetInt("Current", 0);
    }
}
