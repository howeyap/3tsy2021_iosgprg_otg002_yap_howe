﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private float moveSpeed;

    public bool dead;

    [Serializable]
    public struct Arrow
    {
        public string myType;
        public float myChance;
        public Sprite mySprite;
        public Sprite myNakedSprite;
        public Sprite armorSprite;
        public Sprite armorSprite2;
    }

    private GameObject myObjectArrow;

    [SerializeField]
    private Arrow[] arrows;

    [SerializeField]
    private Arrow myArrow;

    public string myDirection;


    [Serializable]
    public struct Direction
    {
        public string rotDirection;
        public float rotation;
    }

    [SerializeField]
    private Direction[] directions;
    
    public float closeThreshold;

    [SerializeField]
    private bool rotateOnStart;

    [SerializeField]
    private int armor;

    private bool threeArmored;
    private void Start()
    {
        myObjectArrow = transform.GetChild(0).gameObject;
        ApplyArrow();
        GameManager.MyInstance.thePlayer.enemySwipe.Add(this);

        if (myArrow.myType == "Yellow")
        {
            closeThreshold = 0f;
            StartCoroutine(YellowRotation());
        }
        if (myArrow.myType == "Armored")
        {
            if (myArrow.armorSprite2 != null)
            {
                threeArmored = true;
                armor = 3;
                Debug.Log("THREE ARMOREEED");
            }
            else
            {
                threeArmored = false;
                armor = 2;
            }
        }
    }
    void Update()
    {
        if (!dead)
        {
            if (GameManager.MyInstance.thePlayer.isDashing)
            {
                transform.Translate(Vector2.down * moveSpeed * 2 * Time.deltaTime);
            }
            else
            {
                transform.Translate(Vector2.down * moveSpeed * Time.deltaTime);
            }
        }

        if (transform.position.y < -2.4 && !dead)
        {
            GameManager.MyInstance.thePlayer.Damage(1);
            GameManager.MyInstance.thePlayer.DeadEnemyByCollision();
        }

        if (transform.position.y < closeThreshold)
        {
            ScaleArrow();
        }
    }

    Arrow RandomizeArrow()
    {
        float chance = 0;
        float randomizer;
        float weight = 0;
        for (int i = 0; i < arrows.Length; i++)
        {
            chance += arrows[i].myChance;
        }

        randomizer = UnityEngine.Random.Range(1f, chance);

        for (int i = 0; i < arrows.Length; i++)
        {
            weight += arrows[i].myChance;
            if (randomizer <= weight)
            {
                return arrows[i];
            }
        }

        return arrows[9];
    }

    void ApplyArrow()
    {
        myArrow = RandomizeArrow();
        if (rotateOnStart)
        {
            int rand = UnityEngine.Random.Range(0, directions.Length);
            myDirection = directions[rand].rotDirection;
            myObjectArrow.transform.rotation = Quaternion.Euler(0, 0, directions[rand].rotation);
        }

        myObjectArrow.GetComponent<SpriteRenderer>().sprite = myArrow.myNakedSprite;
    }

    void ScaleArrow()
    {
        if (!rotateOnStart)
        {
            int rand = UnityEngine.Random.Range(0, directions.Length);
            myDirection = directions[rand].rotDirection;
            myObjectArrow.transform.rotation = Quaternion.Euler(0, 0, directions[rand].rotation);
            rotateOnStart = true;
        }

        if (myObjectArrow != null && !ChangeArmor())
        {
            myObjectArrow.transform.localScale = new Vector3(1.2f, 1.2f, 1);
            myObjectArrow.GetComponent<SpriteRenderer>().sprite = myArrow.mySprite;
        }
    }

    private IEnumerator YellowRotation()
    {
        int rand = 0;
        while (transform.position.y > closeThreshold)
        {
            rand = UnityEngine.Random.Range(0, directions.Length);
            myObjectArrow.transform.rotation = Quaternion.Euler(0, 0, directions[rand].rotation);
            yield return new WaitForSeconds(.2f);
        }
        myDirection = directions[rand].rotDirection;
    }

    public void Dead()
    {
        dead = true;
        Destroy(myObjectArrow);
        Destroy(transform.gameObject, .25f);
        GameManager.MyInstance.thePlayer.enemySwipe.Remove(this);
    }

    public void Damage(int dmg)
    {
        armor -= dmg;
        if (myArrow.myType == "Armored" || armor >1)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + 1, 0);
        }
        
        if (armor <= 0)
        {
            Dead();
        }
    }

    bool ChangeArmor()
    {

        if (myArrow.myType == "Armored")
        {
            if (armor == 2 && threeArmored)
            {
                myObjectArrow.GetComponent<SpriteRenderer>().sprite = myArrow.armorSprite;
                myObjectArrow.transform.localScale = new Vector3(1.4f, 1.4f, 1);
                return true;
            }

            if (armor == 1 && threeArmored)
            {
                myObjectArrow.GetComponent<SpriteRenderer>().sprite = myArrow.armorSprite2;
                myObjectArrow.transform.localScale = new Vector3(1.6f, 1.6f, 1);
                return true;
            }
            else if (armor == 1 && !threeArmored)
            {
                myObjectArrow.GetComponent<SpriteRenderer>().sprite = myArrow.armorSprite;
                myObjectArrow.transform.localScale = new Vector3(1.4f, 1.4f, 1);
                return true;
            }
        }
        return false;
    }
}
