﻿using System;
using System.Collections;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [HideInInspector]
    public Coroutine spawner;

    private Coroutine bossSpawn;

    [Serializable]
    public struct enemy
    {
        public string name;
        public string locale;
        public GameObject prefab;
        public bool boss;
    }

    [SerializeField]
    private enemy[] enemies;

    [SerializeField]
    private Vector3 spawnPos;

    [SerializeField]
    private GameObject cloud;

    private int score;

    private int bossCounter;

    private bool shifted;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnClouds());
        SpawnEnemies();
    }

    private void Awake()
    {
        GameManager.MyInstance.theSpawner = this;
    }

    private void Update()
    {
        score = GameManager.MyInstance.GetScore();
    }

    public void SpawnEnemies()
    {
        spawner = StartCoroutine(SpawnEnemy());
        bossSpawn = StartCoroutine(BossSpawner());
    }

    private IEnumerator SpawnEnemy()
    {
        while (GameManager.MyInstance.thePlayer.GetHealth() > 0)
        {
            int spawnRate = UnityEngine.Random.Range(1, 3);
            //2.45f, 10f, 0
            yield return new WaitForSeconds(spawnRate);

            for (int i = 0; i < enemies.Length; i++)
            {
                if (enemies[i].locale == GameManager.MyInstance.theShifter.currentLocale.name
                    && enemies[i].boss == false)
                {
                    Instantiate(enemies[i].prefab, spawnPos, Quaternion.identity);
                }
            }
        }
    }

    private IEnumerator SpawnClouds()
    {
        float cloudsSpawnRate;
        Vector3 cloudPos;
        while (true)
        {
            cloudsSpawnRate = UnityEngine.Random.Range(1, 3);
            cloudPos = new Vector3(UnityEngine.Random.Range(-.5f, 3f), 6, 0);
            yield return new WaitForSeconds(cloudsSpawnRate);
            Instantiate(cloud, cloudPos, Quaternion.identity);
        }
    }
    private IEnumerator BossSpawner()
    {
        if (shifted)
        {
            shifted = false;
        }
        else
        {
            bossCounter = UnityEngine.Random.Range(25 + score, 28 + score);
        }

        yield return new WaitUntil(() => score > bossCounter);

        StopCoroutine(spawner);
        GameManager.MyInstance.theShifter.CleanShifter();

        yield return new WaitUntil(() => GameManager.MyInstance.thePlayer.enemySwipe.Count <= 0);
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < enemies.Length; i++)
        {
            if (enemies[i].locale == GameManager.MyInstance.theShifter.currentLocale.name
               && enemies[i].boss == true)
            {
                Instantiate(enemies[i].prefab, spawnPos, Quaternion.identity);
            }
        }
        StopCoroutine(BossSpawner());
    }

    public void CleanSpawner()
    {
        StopCoroutine(bossSpawn);
        StopCoroutine(spawner);
        shifted = true;
        bossSpawn = null;
        spawner = null;

        StartCoroutine(AddToCounter());
    }

    private IEnumerator AddToCounter()
    {
        while (shifted)
        {
            bossCounter += 1;
            yield return new WaitForSeconds(1f);
        }
    }
}
