﻿using UnityEngine;

public class Cloud : MonoBehaviour
{
    private float moveSpeed;

    [SerializeField]
    private Sprite[] cloudsSprite;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<SpriteRenderer>().sprite = cloudsSprite[Random.Range(0, cloudsSprite.Length)];
        moveSpeed = Random.Range(2,4);
        Destroy(gameObject, 10f-moveSpeed);
    }

    // Update is called once per frame
    void Update()
    {

        if (GameManager.MyInstance.thePlayer.isDashing)
        {
            transform.Translate(Vector2.down * moveSpeed * 2 * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector2.down * moveSpeed * Time.deltaTime);
        }
    }
}
