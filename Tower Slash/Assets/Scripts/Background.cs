﻿using UnityEngine;

public class Background : MonoBehaviour
{
    public float scrollSpeed;

    [SerializeField]
    private float start;

    [SerializeField]
    private float end;

    [SerializeField]
    private float dashMultiplier;
    void Update()
    {
        if (Player.MyInstance.isDashing)
        {
            transform.Translate(Vector2.down * scrollSpeed * dashMultiplier * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector2.down * scrollSpeed * Time.deltaTime);
        }


        if (transform.position.y < end)
        {
            transform.position = new Vector2(transform.position.x, start);
        }
    }
}
