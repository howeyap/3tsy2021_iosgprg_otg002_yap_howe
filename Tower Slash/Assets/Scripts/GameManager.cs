﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;

    public static GameManager MyInstance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameManager>();
            }

            return instance;
        }
    }

    [SerializeField]
    private Image gameOver;

    [SerializeField]
    private Text scoreText;

    [SerializeField]
    private Text finalScore;

    private int score;

    [SerializeField]
    private GameObject[] hearts;

    public Player thePlayer;

    public Spawner theSpawner;

    public ShiftLocation theShifter;
    // Update is called once per frame
    void Start()
    {
        Time.timeScale = 1;
        score = 0;
        StartCoroutine(ScoreUpdate());
        CheckHealth();
    }

    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public IEnumerator GameOver()
    {
        int newScore = score + PlayerPrefs.GetInt("Coins");
        PlayerPrefs.SetInt("Coins", newScore);
        gameOver.gameObject.SetActive(true);
        scoreText.text = "";
        finalScore.text = score.ToString();

        gameOver.GetComponent<CanvasGroup>().alpha = .2f;
        yield return new WaitForSeconds(.2f);
        gameOver.GetComponent<CanvasGroup>().alpha = .4f;
        yield return new WaitForSeconds(.2f);
        gameOver.GetComponent<CanvasGroup>().alpha = .6f;
        yield return new WaitForSeconds(.2f);
        gameOver.GetComponent<CanvasGroup>().alpha = .8f;
        yield return new WaitForSeconds(.2f);
        gameOver.GetComponent<CanvasGroup>().alpha = 1;
        yield return new WaitForSeconds(.2f);

        Time.timeScale = 0;
        StopCoroutine(GameOver());
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    private IEnumerator ScoreUpdate()
    {
        while (thePlayer.GetHealth() > 0)
        {
            score += 1;
            scoreText.text = score.ToString();
            yield return new WaitForSeconds(1f);
        }
    }

    public void AddScore(int score)
    {
        this.score += score;
    }

    public void RemoveHeart(int index)
    {
        hearts[index].SetActive(false);
    }

    public int GetScore()
    {
        return score;
    }

    public void Resume()
    {
        if (theSpawner.spawner == null)
        {
            theSpawner.SpawnEnemies();
        }

        if (theShifter.shifter == null)
        {
            theShifter.StartShiftTimer();
        }
    }

    void CheckHealth()
    {
        for (int i = 0; i < hearts.Length; i++)
        {
            if (thePlayer.GetHealth() > i)
            {
                hearts[i].SetActive(true);
            }
        }
    }
}
