﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject followTarget;
    private Vector3 targetPos;
    public float moveSpeed;

    [SerializeField]
    private float offset;
    // Update is called once per frame
    void Update()
    {
        targetPos = new Vector3(transform.position.x, followTarget.transform.position.y - offset, transform.position.z);
        transform.position = Vector3.Lerp(transform.position, targetPos, moveSpeed * Time.deltaTime);
    }
}
