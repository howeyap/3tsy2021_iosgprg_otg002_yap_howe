﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrostGiant : MonoBehaviour
{
    [SerializeField]
    private GameObject iceBall;

    private bool dead;

    [SerializeField]
    private int health;

    [SerializeField]
    private List<Enemy> iceBalls;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Attack());
    }
    private void Update()
    {
        if (transform.position.y > 3.5f)
        {
            transform.Translate(Vector3.down * 2 * Time.deltaTime);
        }

        if (iceBalls != null)
        {
            CheckDamage();
        }
    }

    void CheckDamage()
    {
        for (int i = 0; i < iceBalls.Count; i++)
        {
            if (iceBalls[i].dead)
            {
                Damage(1);
                iceBalls.RemoveAt(i);
            }
        }
    }

    private IEnumerator Attack()
    {
        yield return new WaitForSeconds(4f);
        while (!dead && Player.MyInstance.getHealth() > 0)
        {
            SpawnBall();
            yield return new WaitForSeconds(.5f);
            SpawnBall();
            yield return new WaitForSeconds(.7f);
            SpawnBall();
            yield return new WaitForSeconds(3f);
        }
    }

    void SpawnBall()
    {
        iceBalls.Add(Instantiate(iceBall, transform.position, Quaternion.identity).GetComponent<Enemy>());
    }

    void Damage(int dmg)
    {
        health -= dmg;
        if (health <= 0)
        {
            Die();
        }
    }
    
    void Die()
    {
        GameManager.MyInstance.SpawnEnemies();
        Destroy(gameObject, .5f);
    }   
}
