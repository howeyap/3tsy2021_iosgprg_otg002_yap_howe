﻿using UnityEngine;

public class TowerScroll : MonoBehaviour
{
    public float scrollSpeed;

    [SerializeField]
    private float start;

    [SerializeField]
    private float end;

    [SerializeField]
    private float dashMultiplier;
    void Update()
    {
        if (GameManager.MyInstance.thePlayer.isDashing)
        {
            transform.Translate(Vector2.down * scrollSpeed * dashMultiplier * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector2.down * scrollSpeed * Time.deltaTime);
        }


        if (transform.position.y < end)
        {
            transform.position = new Vector2(transform.position.x, start);
        }
    }
}
