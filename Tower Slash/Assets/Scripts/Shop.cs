﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    [SerializeField]
    private Player player;

    [SerializeField]
    private int costExtra;

    [SerializeField]
    private Text textExtra;

    [SerializeField]
    private Text currentHealth;

    [SerializeField]
    private Text coins;

    [Serializable]
    public struct Item
    {
        public string name;
        public int id;
        public int cost;
        public Sprite image;
    }

    [SerializeField]
    private Item[] items;

    private int page;

    [SerializeField]
    private Image costumeImage;

    [SerializeField]
    private Text costumeName;

    [SerializeField]
    private Text costumeCost;
    private void Start()
    {
        page = 0;
        UpdatePage();
        UpdateHealth();

        if (PlayerPrefs.GetInt("Health") < 1)
        {
            PlayerPrefs.SetInt("Health", 1);
        }

        //default character
        if (PlayerPrefs.GetInt("Jason") != 1)
        {
            PlayerPrefs.SetInt("Jason", 1);
        }
        
    }

    public void PurchaseCostume()
    {
        if (PlayerPrefs.GetInt(items[page].name) != 1 
            && PlayerPrefs.GetInt("Coins") >= items[page].cost)
        {
            PlayerPrefs.SetInt(items[page].name, 1);
            PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins")-items[page].cost);
        }

        if (PlayerPrefs.GetInt(items[page].name) == 1)
        {
            PlayerPrefs.SetInt("Current", items[page].id);
        }

        UpdatePage();
    }
    public void PurchaseHealth()
    {
        if (PlayerPrefs.GetInt("Health") < 4 && PlayerPrefs.GetInt("Coins") >= costExtra)
        {
            PlayerPrefs.SetInt("Health", PlayerPrefs.GetInt("Health")+1);
            PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins") - costExtra);
        }

        UpdateHealth();
    }

    public void Next()
    {
        if (items.Length > page+1)
        {
            page++;
            UpdatePage();
        }
    }

    public void Prev()
    {
        if (0 < page)
        {
            page--;
            UpdatePage();
        }
    }

    private void UpdatePage()
    {
        costumeImage.sprite = items[page].image;

        costumeName.text = items[page].name;

        if (PlayerPrefs.GetInt("Current") == items[page].id)
        {
            costumeCost.text = "Equipped";
        }
        else if (PlayerPrefs.GetInt(items[page].name) == 1)
        {
            costumeCost.text = "Switch";
        }
        else
        {
            costumeCost.text = items[page].cost.ToString();
        }

        if (coins != null)
        {
            coins.text = PlayerPrefs.GetInt("Coins").ToString();
        }
    }

    private void UpdateHealth()
    {
        currentHealth.text = "Health: " + PlayerPrefs.GetInt("Health");

        if (PlayerPrefs.GetInt("Health") > 3)
        {
            textExtra.text = "Maxxed";
        }
        else
        {
            textExtra.text = costExtra.ToString();
        }

        if (coins != null)
        {
            coins.text = PlayerPrefs.GetInt("Coins").ToString();
        }
    }
}
