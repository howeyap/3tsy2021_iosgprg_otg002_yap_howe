﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Build;
using UnityEngine;

public class ShiftLocation : MonoBehaviour
{
    private int shiftCounter;

    private int score;

    private bool bossSpawned;

    [HideInInspector]
    public Coroutine shifter;

    [SerializeField]
    private float fadeSpeed;

    public location currentLocale;

    [SerializeField]
    private SpriteRenderer tower;

    [Serializable]
    public struct location
    {
        public string name;
        public SpriteRenderer bg;
        public Color color;
    }

    [SerializeField]
    private List<location> locations; 
    // Start is called before the first frame update
    void Start()
    {
        GameManager.MyInstance.theShifter = this;

        StartShiftTimer();
        StartCoroutine(ChooseArea());
    }

    // Update is called once per frame
    void Update()
    {
        score = GameManager.MyInstance.GetScore();
    }

    private IEnumerator FadeToBlack(SpriteRenderer current, bool changeAlpha)
    {
        for (float i = 1f; i >= 0; i-=.1f)
        {
            Color c = current.material.color;

            c.r = i;
            c.g = i;
            c.b = i;

            current.material.color = c;
            yield return new WaitForSeconds(fadeSpeed);
        }

        if (changeAlpha)
        {
            current.material.color = Color.clear;
        }
    }

    private IEnumerator FadeIn(SpriteRenderer current)
    {
        current.material.color = Color.black;
        for (float i = 0; i < 1; i+=.1f)
        {
            Color c = current.material.color;

            c.r = i;
            c.g = i;
            c.b = i;

            current.material.color = c;
            yield return new WaitForSeconds(fadeSpeed);
        }
    }
    private IEnumerator SpecificColor(Color newColor, SpriteRenderer current)
    {
        current.material.color = new Color(newColor.r / 6, newColor.g / 6, newColor.b / 6);
        yield return new WaitForSeconds(.1f);
        current.material.color = new Color(newColor.r / 4, newColor.g / 4, newColor.b / 4);
        yield return new WaitForSeconds(.1f);
        current.material.color = new Color(newColor.r/2, newColor.g/2, newColor.b/2);
        yield return new WaitForSeconds(.1f);
        current.material.color = newColor;
    }

    private IEnumerator ShiftLocale()
    {
        if (bossSpawned)
        {
            bossSpawned = false;
        }
        else
        {
            shiftCounter = UnityEngine.Random.Range(10, 30);
        }

        yield return new WaitUntil(() => score > shiftCounter);

        if (!bossSpawned)
        {
            GameManager.MyInstance.theSpawner.CleanSpawner();
        }

        yield return new WaitUntil(() => GameManager.MyInstance.thePlayer.enemySwipe.Count <= 0);
        yield return new WaitForSeconds(1f);
        StartCoroutine(ChooseArea());
    }

    public void CleanShifter()
    {
        bossSpawned = true;
        StopCoroutine(shifter);
        shifter = null;

        StartCoroutine(AddToCounter());
    }

    private IEnumerator ChooseArea()
    {
        int rand = UnityEngine.Random.Range(0, locations.Count);

        if (currentLocale.bg != null)
        {
            StartCoroutine(FadeToBlack(currentLocale.bg,true));
            locations.Add(currentLocale);
            currentLocale.bg.sortingOrder = -2;
        }

        currentLocale = locations[rand];
        StartCoroutine(FadeToBlack(tower, false));
        yield return new WaitForSeconds(1f);
        currentLocale.bg.sortingOrder = -1;
        StartCoroutine(FadeIn(locations[rand].bg));
        StartCoroutine(SpecificColor(locations[rand].color, tower));

        locations.RemoveAt(rand);
        yield return new WaitForSeconds(2f);

        GameManager.MyInstance.Resume();
    }

    public void StartShiftTimer()
    {
        shifter = StartCoroutine(ShiftLocale());
    }

    private IEnumerator AddToCounter()
    {
        while (bossSpawned)
        {
            shiftCounter += 1;
            yield return new WaitForSeconds(1f);
        }
    }
}
