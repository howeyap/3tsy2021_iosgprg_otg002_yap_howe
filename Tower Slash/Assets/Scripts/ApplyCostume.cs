﻿using System;
using UnityEngine;

public class ApplyCostume : MonoBehaviour
{
    private Animator myAnim;

    [Serializable]
    public struct Costume
    {
        public int id;
        public RuntimeAnimatorController anim;
    }

    [SerializeField]
    private Costume[] costumes;
    // Start is called before the first frame update
    void Start()
    {
        myAnim = GameManager.MyInstance.thePlayer.GetComponent<Animator>();
        ChangeCostume();
    }

    void ChangeCostume()
    {
        for (int i = 0; i < costumes.Length; i++)
        {
            if (PlayerPrefs.GetInt("Current") == costumes[i].id)
            {
                myAnim.runtimeAnimatorController = costumes[i].anim;
            }
        }
    }
}
