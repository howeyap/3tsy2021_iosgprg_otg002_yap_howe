﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    private bool dead;

    [SerializeField]
    private int health;

    [SerializeField]
    private List<Enemy> attacks;

    private Animator myAnim;

    [SerializeField]
    private GameObject weapon;

    [SerializeField]
    private int numberOfAttacks;

    [SerializeField]
    private float attackIntervals;

    [SerializeField]
    private float seriesAttackIntervals;
    // Start is called before the first frame update
    void Start()
    {
        myAnim = GetComponent<Animator>();
        StartCoroutine(Attack());
    }
    private void Update()
    {
        if (transform.position.y > 3.5f)
        {
            transform.Translate(Vector3.down * 2 * Time.deltaTime);
        }

        if (attacks != null)
        {
            CheckDamage();
        }
    }

    void CheckDamage()
    {
        for (int i = 0; i < attacks.Count; i++)
        {
            if (attacks[i].dead)
            {
                Damage(1);
                attacks.RemoveAt(i);
            }
        }
    }

    private IEnumerator Attack()
    {
        yield return new WaitForSeconds(4f);
        while (!dead && GameManager.MyInstance.thePlayer.GetHealth() > 0)
        {
            for (int i = 0; i < numberOfAttacks; i++)
            {
                SpawnBall();
                myAnim.SetTrigger("Attack");
                yield return new WaitForSeconds(attackIntervals);
            }
            yield return new WaitForSeconds(seriesAttackIntervals);
        }
    }

    void SpawnBall()
    {
        attacks.Add(Instantiate(weapon, transform.position, Quaternion.identity).GetComponent<Enemy>());
    }

    void Damage(int dmg)
    {
        health -= dmg;
        if (health <= 0)
        {
            Die();
        }
    }
    
    void Die()
    {
        GameManager.MyInstance.Resume();
        Destroy(gameObject, .5f);
    }   
}
